$(document).ready(function () {
  $(
    ".jfilter-content, .jfilter-showct, .jfilter-show_price, .jfilter-show_type, .jfilter-show_prefix, .jfilter-show_avoidance, .jfilter-show_score, .jfilter-show_pow"
  ).css("display", "none");

  $(".jfilter-funnel").click(function () {
    $(".jfilter-content").toggle();
  });

  $(".jfilter-close").click(function () {
    $(".jfilter-content").css("display", "none");
  });

  const $funnel = $(".jfilter-content");
  $(document).mouseup((e) => {
    if (!$funnel.is(e.target) && $funnel.has(e.target).length === 0) {
      $funnel.hide();
    }
  });

  $(".jfilter-network").click(function (e) {
    $(".jfilter-showct").toggle();
    e.stopPropagation();
  });

  const $network = $(".jfilter-showct");
  $(document).mouseup((e) => {
    if (!$network.is(e.target) && $network.has(e.target).length === 0) {
      $network.hide();
    }
  });

  $(".jfilter-price").click(function (e) {
    $(".jfilter-show_price").toggle(e);
    e.stopPropagation();
  });

  const $price = $(".jfilter-show_price");
  $(document).mouseup((e) => {
    if (!$price.is(e.target) && $price.has(e.target).length === 0) {
      $price.hide();
    }
  });

  $(".jfilter-type").click(function (e) {
    $(".jfilter-show_type").toggle();
    e.stopPropagation();
  });

  const $type = $(".jfilter-show_type");
  $(document).mouseup((e) => {
    if (!$type.is(e.target) && $type.has(e.target).length === 0) {
      $type.hide();
    }
  });

  $(".jfilter-prefix").click(function (e) {
    $(".jfilter-show_prefix").toggle();
    e.stopPropagation();
  });

  const $prefix = $(".jfilter-show_prefix");
  $(document).mouseup((e) => {
    if (!$prefix.is(e.target) && $prefix.has(e.target).length === 0) {
      $prefix.hide();
    }
  });

  $(".jfilter-avoidance").click(function (e) {
    $(".jfilter-show_avoidance").toggle();
    e.stopPropagation();
  });

  const $avoidance = $(".jfilter-show_avoidance");
  $(document).mouseup((e) => {
    if (!$avoidance.is(e.target) && $avoidance.has(e.target).length === 0) {
      $avoidance.hide();
    }
  });

  $(".jfilter-score").click(function (e) {
    $(".jfilter-show_score").toggle();
    e.stopPropagation();
  });

  const $score = $(".jfilter-show_score");
  $(document).mouseup((e) => {
    if (!$score.is(e.target) && $score.has(e.target).length === 0) {
      $score.hide();
    }
  });

  $(".jfilter-pow").click(function (e) {
    $(".jfilter-show_pow").toggle();
    e.stopPropagation();
  });

  const $pow = $(".jfilter-show_pow");
  $(document).mouseup((e) => {
    if (!$pow.is(e.target) && $pow.has(e.target).length === 0) {
      $pow.hide();
    }
  });

  $("#check_menu").change(function () {
    if (this.checked) {
      $("main").addClass("none");
    } else {
      $("main").removeClass("none");
    }
  });
});
